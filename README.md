# About
A Magento 2 module that synchronise products and orders with B1 system. Module currently under development.

# Installation
1. Download / clone repository
1. Copy `B1` directory to your magento2 store directory `magento2/app/code`
1. To view enabled and disabled modules, run command `bin/magento module:status` in your magento2 directory. If you don't see `B1_Accounting` in list, you probably copied B1 module to wrong place. If module is already enabled you should see it in List of enabled modules.
1. To enable `B1_Accounting` module, run command `bin/magento module:enable B1_Accounting`
1. Once module enabled, run command `bin/magento setup:upgrade` so that module get properly registered.

# Development
Useful information for module development.

## Useful magento commands
These commands must be run in `magento2` directory.


To view enabled and disabled modules run cmd:
```
bin/magento module:status
```

To enable this module run cmd:
```
bin/magento module:enable B1_Accounting
```

To disable this module run cmd:
```
bin/magento module:disable B1_Accounting
```

To make sure that the enabled modules are properly registered, run cmd:
```
bin/magento setup:upgrade
```

To make changes visible after editing module code, run cmd:
```
bin/magento cache:clean
```

If Magento stop working (not loading page and etc...), run cmd:
```
bin/magento setup:di:compile
```

To create the Magento crontab, run cmd:
```
bin/magento cron:install
```

To view b1 cron job list, run mysql query:
```
SELECT * from cron_schedule WHERE job_code LIKE "%b1_sync%";
```

## Useful links
- https://techstorm.io/how-to-install-magento-2-4-x-with-apache-on-ubuntu-20-04/

## Unit Testing
To run unit tests, run cmd in `magento2` directory:
```
./vendor/bin/phpunit -c dev/tests/unit/phpunit.xml.dist app/code/B1/Accounting/Test/Unit
```

Read more about testing: https://devdocs.magento.com/guides/v2.4/test/unit/unit_test_execution_cli.html