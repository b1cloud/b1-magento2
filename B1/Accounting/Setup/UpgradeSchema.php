<?php

namespace B1\Accounting\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;


class UpgradeSchema implements UpgradeSchemaInterface
{
    private $_configWriter;

    public function __construct(
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter)
    {
        $this->_configWriter = $configWriter;
    }

	public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if(version_compare($context->getVersion(), '1.0.0', '<')) {
            $connection = $setup->getConnection();

            $tableNameProduct = $setup->getTable('catalog_product_entity');
            $columnNameProduct = 'b1_reference_id';

            if ($connection->tableColumnExists($tableNameProduct, $columnNameProduct) === false) {

                $connection->addColumn($tableNameProduct, $columnNameProduct, array(
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'default' => null,
                    'comment' => 'B1',
                ));
            }


            $tableNameSales = $setup->getTable('sales_order');
            $columnNameSales = 'b1_reference_id';

            if ($connection->tableColumnExists($tableNameSales, $columnNameSales) === false) {

                $connection->addColumn($tableNameSales, $columnNameSales, array(
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'default' => null,
                    'comment' => 'B1',
                ));
            }
        }

        if(version_compare($context->getVersion(), '1.0.1') < 0) {
            $logTableName = $setup->getTable( \B1\Accounting\Model\Log::logsTableName() );
            if ($setup->getConnection()->isTableExists($logTableName) != true) {
                $table = $setup->getConnection()
                    ->newTable($logTableName)
                    ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ], 'id')
                    ->addColumn(
                        'created_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null,
                        ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                        'Created At'
                    )
                    ->addColumn('is_success', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, [
                        'nullable' => false
                    ], 'is_success')
                    ->addColumn('debug_info', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, null, [
                        'nullable' => false
                    ], 'debug_info')
                    ->setComment('logs table');
                $setup->getConnection()->createTable($table);
            }
        }

        if(version_compare($context->getVersion(), '1.0.2') < 0) {
            $this->_configWriter->save(\B1\Accounting\Observer\MyObserver::CONFIG_PATH_ORDER_SYNC_TO, null);
        }

        if(version_compare($context->getVersion(), '1.0.3') < 0) {
            $logTableName = $setup->getTable( \B1\Accounting\Model\Validation::logsTableName() );
            if ($setup->getConnection()->isTableExists($logTableName) != true) {
                $table = $setup->getConnection()
                    ->newTable($logTableName)
                    ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ], 'id')
                    ->addColumn(
                        'created_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null,
                        ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                        'Created At'
                    )
                    ->addColumn('debug_info', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, null, [
                        'nullable' => false
                    ], 'debug_info')
                    ->setComment('validation logs table');
                $setup->getConnection()->createTable($table);
            }
        }

        $setup->endSetup();
	}
}
