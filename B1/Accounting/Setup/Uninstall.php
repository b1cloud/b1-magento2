<?php
/**
 * Created by PhpStorm.
 * User: JohnMirro
 * Date: 29.09.2021
 * Time: 16:57
 */

namespace B1\Accounting\Setup;

use Magento\Framework\Setup\UninstallInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class Uninstall implements UninstallInterface
{
    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $connection = $setup->getConnection();

        $tableNameProduct = $setup->getTable('catalog_product_entity');
        $columnNameProduct = 'b1_reference_id';

        if ($connection->tableColumnExists($tableNameProduct, $columnNameProduct)) {

            $connection->dropColumn($tableNameProduct, $columnNameProduct);
        }

        $tableNameSales = $setup->getTable('sales_order');
        $columnNameSales = 'b1_reference_id';
        if ($connection->tableColumnExists($tableNameSales, $columnNameSales)) {

            $connection->dropColumn($tableNameProduct, $columnNameSales);
        }

        $logTableName = $setup->getTable(\B1\Accounting\Model\Log::logsTableName());
        if ($connection->isTableExists($logTableName)) {
            $connection->dropTable($logTableName);
        }

        $installer->endSetup();
    }
}
