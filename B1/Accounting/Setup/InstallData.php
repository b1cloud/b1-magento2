<?php

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    private $_configWriter;
    private $_logger;

    public function __construct(
        \Psr\Log\LoggerInterface  $loggerInterface,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter)
    {
        $this->_logger = $loggerInterface;
        $this->_configWriter = $configWriter;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $this->_configWriter->save(\B1\Accounting\Observer\MyObserver::CONFIG_PATH_ORDER_SYNC_FROM, date('Y-m-d H:i:s'));
        $this->_configWriter->save(\B1\Accounting\Observer\MyObserver::CONFIG_PATH_ORDER_SYNC_TO, null);
        $setup->endSetup();
    }
}
