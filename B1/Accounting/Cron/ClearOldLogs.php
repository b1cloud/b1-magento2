<?php

namespace B1\Accounting\Cron;

class ClearOldLogs
{

    private $_myObserver;

    public function __construct(
        \B1\Accounting\Observer\MyObserver $myObserver
    )
    {
        $this->_myObserver = $myObserver;
    }

    public function execute()
    {
        $this->_myObserver->clearOldLogs();
    }
}
