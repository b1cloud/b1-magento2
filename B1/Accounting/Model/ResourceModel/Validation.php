<?php

namespace B1\Accounting\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;


class Validation extends AbstractDb
{
    public $table;


    protected function _construct()
    {
        $this->table = $this->getTable(\B1\Accounting\Model\Validation::logsTableName());
        $this->_init($this->table, 'id');

    }

    public function saveLog($debug_info)
    {

        try {
            $connection = $this->getConnection();
            $tableName = $connection->getTableName($this->table);
            $connection->insert($tableName, ['debug_info' => $debug_info]);

        } catch (\Exception $e) {
        }
    }

    public function clearLogs()
    {
        try {
            $connection = $this->getConnection();
            $connection->query("TRUNCATE TABLE ".$this->table."");

        } catch (\Exception $e) {
        }
    }

    public function hasRecords()
    {
        try {
            $connection = $this->getConnection();
            $tableName = $connection->getTableName($this->table);
            $select = $connection->select();
            $select->from($tableName, 'COUNT(*)');
            $result = (int)$connection->fetchOne($select);
            return $result > 0;

        } catch (\Exception $e) {
            return false;
        }
    }

}
