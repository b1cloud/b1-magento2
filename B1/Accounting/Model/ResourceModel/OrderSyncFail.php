<?php

namespace B1\Accounting\Model\ResourceModel;

class OrderSyncFail extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {

        $this->_init( $this->getTable('sales_order_items'), 'item_id');
    }
}