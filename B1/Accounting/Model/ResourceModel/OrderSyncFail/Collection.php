<?php

namespace B1\Accounting\Model\ResourceModel\OrderSyncFail;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'item_id';
    public function _construct()
    {
        $this->_init('B1\Accounting\Model\OrderSyncFail', 'B1\Accounting\Model\ResourceModel\OrderSyncFail');
    }
}