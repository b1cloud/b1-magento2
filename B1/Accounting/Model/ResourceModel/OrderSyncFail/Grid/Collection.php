<?php

namespace B1\Accounting\Model\ResourceModel\OrderSyncFail\Grid;

use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Api\Search\AggregationInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection implements SearchResultInterface {

    protected $aggregations;

    public function __construct(
    \Magento\Framework\App\RequestInterface $request, \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory, \Psr\Log\LoggerInterface $logger, \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy, \Magento\Framework\Event\ManagerInterface $eventManager, $mainTable, $eventPrefix, $eventObject, $resourceModel, $model = \Magento\Framework\View\Element\UiComponent\DataProvider\Document::class, \Magento\Framework\DB\Adapter\AdapterInterface $connection = null, \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        $this->request = $request;
        $this->_eventPrefix = $eventPrefix;
        $this->_eventObject = $eventObject;
        $this->_init($model, $resourceModel);
        $this->setMainTable($mainTable);
        parent::__construct(
                $entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource
        );
    }

    public function getAggregations() {
        return $this->aggregations;
    }

    public function setAggregations($aggregations) {
        $this->aggregations = $aggregations;
        return $this;
    }

    public function getAllIds($limit = null, $offset = null) {
        return $this->getConnection()->fetchCol($this->_getAllIdsSelect($limit, $offset), $this->_bindParams);
    }

    public function getSearchCriteria() {
        return null;
    }

    public function setSearchCriteria(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria = null) {
        return $this;
    }

    public function getTotalCount() {
        return $this->getSize();
    }

    public function setTotalCount($totalCount) {
        return $this;
    }

    public function setItems(array $items = null) {
        return $this;
    }

    protected function _initSelect() {
        parent::_initSelect();

        $search = $this->request->getParam('search');
        if (isset($search) && $search != '') {

            $this->addFieldToFilter(
                    array(
                'main_table.name'
                    ), array(
                array('like' => '%' . $search . '%')
                    )
            );
        }
    }

    protected function _beforeLoad()
    {
        parent::_beforeLoad();
        $this->addFieldToFilter('b1_reference_id',['null'=>true]);
        // Shows orders that are completed and not sync yet.
        // $this->addFieldToFilter('b1_reference_id',['null'=>true])->addFieldToFilter('status',['complete']);
        return $this;
    }

}
