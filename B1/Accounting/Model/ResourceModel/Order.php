<?php

namespace B1\Accounting\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Order extends AbstractDb
{
    public $table;

    protected function _construct()
    {
        $this->table = $this->getTable('sales_order');
        $this->_init($this->table , 'entity_id');
    }

    public function resetOrderReferenceId()
    {
        $this->getConnection()->query(
            "UPDATE ".$this->table."
                SET `b1_reference_id`=:r",
            [
                'r' => null,
            ]);
    }

    public function findAllToSync($orderSyncFrom, $statusName, $orderSyncDateTo)
    {
        if(!empty($orderSyncDateTo)) {
            return $this->getConnection()->fetchAll(
                "SELECT *
               FROM ".$this->table."
               WHERE b1_reference_id IS NULL AND (created_at >= :f AND created_at <= :t) AND (status=:os OR entity_id IN (SELECT parent_id FROM ".$this->getTable('sales_order_status_history')." WHERE status=:os)) LIMIT 100",
                [
                    'os' => $statusName,
                    'f' => $orderSyncFrom,
                    't' => $orderSyncDateTo,
                ]);
        } else
        return $this->getConnection()->fetchAll(
            "SELECT *
               FROM ".$this->table."
               WHERE b1_reference_id IS NULL AND created_at>=:f AND (status=:os OR entity_id IN (SELECT parent_id FROM ".$this->getTable('sales_order_status_history')." WHERE status=:os)) LIMIT 100",
            [
                'os' => $statusName,
                'f' => $orderSyncFrom,
            ]);
    }

    public function updateB1ReferenceId($orderId, $referenceId)
    {
        if ($referenceId == null) {
            $this->getConnection()->query(
                "UPDATE ".$this->table."
                    SET b1_reference_id = NULL
                    WHERE entity_id = :o",
                [
                    'o' => $orderId,
                ]);
        } else {
            $this->getConnection()->query(
                "UPDATE ".$this->table."
                    SET b1_reference_id = :r
                    WHERE entity_id = :o",
                [
                    'r' => $referenceId,
                    'o' => $orderId,
                ]);
        }
    }
}
