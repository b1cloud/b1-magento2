<?php

namespace B1\Accounting\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Product extends AbstractDb
{
    public $table;

    protected function _construct()
    {
        $this->table = $this->getTable('catalog_product_entity');
        $this->_init($this->table , 'entity_id');
    }

    public function resetAllB1ReferenceId()
    {
        $this->getConnection()->query(
            "UPDATE ".$this->table."
                SET `b1_reference_id`=:r",
            [
                'r' => null,
            ]);
    }

    public function updateCode($code, $referenceId)
    {

        $this->getConnection()->query(
            "UPDATE ".$this->table."
                SET `b1_reference_id`=:r
                WHERE `sku`=:id",
            [
                'r' => (int) $referenceId,
                'id' => $code,
            ]);
    }

    public function updateQuantity($product)
    {
        $id = (int) $product['id'];
        $quantity = (int) $product['quantity'];

        $conn = $this->getConnection();

        $stockStatus = $quantity > 0 ? 1 : 0;

        $conn->query(
            "UPDATE ".$this->getTable('cataloginventory_stock_item')." s
                LEFT OUTER JOIN ".$this->table." p
                ON s.`product_id` = p.`entity_id`
                SET s.`qty` = :q, s.`is_in_stock` = :status
                WHERE p.`b1_reference_id`=:id",
            [
                'id' => $id,
                'q' => $quantity,
                'status' => $stockStatus,
            ]);
        // is_in_stock

        $conn->query(
            "UPDATE ".$this->getTable('cataloginventory_stock_status')." s
                LEFT OUTER JOIN ".$this->table." p
                ON s.`product_id` = p.`entity_id`
                SET s.`qty` = :q, s.`stock_status` = :status
                WHERE p.`b1_reference_id`=:id",
            [
                'id' => $id,
                'q' => $quantity,
                'status' => $stockStatus,
            ]);
        // stock_status
    }

    public function updateName($product)
    {
        $id = (int) $product['id'];
        $name = $product['name'];

        $this->getConnection()->query(
            "UPDATE ".$this->getTable('catalog_product_entity_varchar')." cp
                LEFT OUTER JOIN ".$this->table." p
                ON cp.`entity_id` = p.`entity_id`
                SET cp.`value` = :q
                WHERE p.`b1_reference_id`=:id",
            [
                'id' => $id,
                'q' => $name,
            ]);
    }

    public function updatePrice($product)
    {
        $id = (int) $product['id'];
        $price = $product['priceWithoutVat'];

        $this->getConnection()->query(
            "UPDATE ".$this->getTable('catalog_product_entity_decimal')." cp
                LEFT OUTER JOIN ".$this->table." p
                ON cp.`entity_id` = p.`entity_id`
                SET cp.`value` = :q
                WHERE p.`b1_reference_id`=:id",
            [
                'id' => $id,
                'q' => $price,
            ]);
    }

    public function getPrice($productId)
    {
        $result = $this->getConnection()->fetchOne(
            "SELECT cp.`value`
              FROM ".$this->getTable('catalog_product_entity_decimal')." cp
                WHERE cp.`entity_id`=$productId",
            [
                'id' => $productId
            ]);
        return $result;
    }

    public function fetchAllItems()
    {
        return $this->getConnection()->fetchAll(
            "SELECT entity_id, sku
                FROM ".$this->table."
                WHERE `b1_reference_id` is null AND sku != ' ' AND sku is not null LIMIT 100"
        );
    }

    public function fetchAllItemCount()
    {
        return $this->getConnection()->fetchAll(
            "SELECT count(*) as count
                FROM ".$this->table."
                WHERE `b1_reference_id` is null AND sku != ' ' AND sku is not null");
    }

}