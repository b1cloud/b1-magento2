<?php

namespace B1\Accounting\Model\ResourceModel\Log;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init('B1\Accounting\Model\Log', 'B1\Accounting\Model\ResourceModel\Log');
    }
}
