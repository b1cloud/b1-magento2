<?php

namespace B1\Accounting\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;


class Log extends AbstractDb
{
    public $table;


    protected function _construct()
    {
        $this->table = $this->getTable(\B1\Accounting\Model\Log::logsTableName());
        $this->_init($this->table, 'id');

    }

    public function saveLog($is_success, $debug_info)
    {

        try {
            $connection = $this->getConnection();
            $tableName = $connection->getTableName($this->table);
            $connection->insert($tableName, [
                'is_success' => $is_success,
                'debug_info' => json_encode($debug_info, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT),
            ]);

        } catch (\Exception $e) {
        }
    }

    public function clearLogs()
    {

        try {
            $time = date('Y-m-d H:i:s', strtotime('-7 day'));

            $connection = $this->getConnection();

            $connection->query("DELETE FROM ".$this->table."  WHERE `created_at` < '{$time}'");

        } catch (\Exception $e) {
        }
    }

}
