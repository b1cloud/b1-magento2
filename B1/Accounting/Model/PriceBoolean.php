<?php

namespace B1\Accounting\Model;

class PriceBoolean implements \Magento\Framework\Data\OptionSourceInterface
{

    public function toOptionArray()
    {
        return [
            [
                'value' => 0, 
                'label' => __('No')
            ],
            [
                'value' => 1, 
                'label' => __('Sync price without VAT')
            ],
        ];
    }

}