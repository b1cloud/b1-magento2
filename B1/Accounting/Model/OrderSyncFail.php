<?php

namespace B1\Accounting\Model;

class OrderSyncFail extends \Magento\Framework\Model\AbstractModel
{
    public function _construct()
    {
        $this->_init('B1\Accounting\Model\ResourceModel\OrderSyncFail');
    }
}
