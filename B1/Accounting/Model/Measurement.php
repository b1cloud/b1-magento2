<?php

namespace B1\Accounting\Model;

class Measurement implements \Magento\Framework\Data\OptionSourceInterface
{

    public function toOptionArray()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $model = $objectManager->create('\B1\Accounting\Observer\MyObserver');
        $measurements = $model->getImportMeasurement();

        $measurementToOption = function ($m) {
            return [
                'value' => $m['id'],
                'label' => __($m['name']),
            ];
        };

        $optionArray = array_map($measurementToOption, $measurements);

        return $optionArray;
    }
}
