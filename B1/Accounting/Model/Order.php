<?php

namespace B1\Accounting\Model;

use Magento\Framework\Model\AbstractModel;

class Order extends AbstractModel
{
    const CONFIG_PATH_SHOP_ID = 'b1_configuration_section/b1_main_config_group/shop_id';
    const CONFIG_PATH_ENABLE_SYNC_INVOICES = 'b1_configuration_section/b1_main_config_group/enable_sync_invoices';
    const CONFIG_PATH_ENABLE_QUANTITY_SYNC = 'b1_configuration_section/b1_main_config_group/enable_quantity_sync';
    const CONFIG_PATH_ENABLE_NAME_SYNC = 'b1_configuration_section/b1_main_config_group/enable_name_sync';
    const CONFIG_PATH_ENABLE_PRICE_SYNC = 'b1_configuration_section/b1_main_config_group/enable_price_sync';
    const CONFIG_PATH_ENABLE_WRITEOFF = 'b1_configuration_section/b1_main_config_group/enable_write_off';
    const CONFIG_PATH_ORDER_SYNC_FROM = 'b1_configuration_section/b1_main_config_group/order_sync_from';
    const CONFIG_PATH_ORDER_SYNC_TO = 'b1_configuration_section/b1_main_config_group/order_sync_to';

    const CONFIG_PATH_MAPPING_ORDER_DATE = 'b1_mapping_section/b1_datafields_group/orderdate';
    const CONFIG_PATH_MAPPING_ORDER_NO = 'b1_mapping_section/b1_datafields_group/orderno';
    const CONFIG_PATH_MAPPING_CURRENCY = 'b1_mapping_section/b1_datafields_group/currency';
    const CONFIG_PATH_MAPPING_SHIPPING_AMOUNT = 'b1_mapping_section/b1_datafields_group/shippingamount';
    const CONFIG_PATH_MAPPING_GIFT = 'b1_mapping_section/b1_datafields_group/gift';
    const CONFIG_PATH_MAPPING_DISCOUNT = 'b1_mapping_section/b1_datafields_group/discount';
    const CONFIG_PATH_MAPPING_TOTAL = 'b1_mapping_section/b1_datafields_group/total';
    const CONFIG_PATH_MAPPING_ORDER_EMAIL = 'b1_mapping_section/b1_datafields_group/orderemail';
    const CONFIG_PATH_MAPPING_INVOICE_SERIES = 'b1_mapping_section/b1_datafields_group/invoiceseries';
    const CONFIG_PATH_MAPPING_INVOICE_NUMBER = 'b1_mapping_section/b1_datafields_group/invoicenumber';
    const CONFIG_PATH_MAPPING_BILLING_FIRST_NAME = 'b1_mapping_section/b1_datafields_group/billingfirstname';
    const CONFIG_PATH_MAPPING_BILLING_LAST_NAME = 'b1_mapping_section/b1_datafields_group/billinglastname';
    const CONFIG_PATH_MAPPING_BILLING_IS_COMPANY = 'b1_mapping_section/b1_datafields_group/billingiscompany';
    const CONFIG_PATH_MAPPING_BILLING_VAT_CODE = 'b1_mapping_section/b1_datafields_group/billingvatcode';
    const CONFIG_PATH_MAPPING_BILLING_ADDRESS = 'b1_mapping_section/b1_datafields_group/billingaddress';
    const CONFIG_PATH_MAPPING_BILLING_CITY = 'b1_mapping_section/b1_datafields_group/billingcity';
    const CONFIG_PATH_MAPPING_BILLING_POST_CODE = 'b1_mapping_section/b1_datafields_group/billingpostcode';
    const CONFIG_PATH_MAPPING_BILLING_COUNTRY = 'b1_mapping_section/b1_datafields_group/billingcountry';
    const CONFIG_PATH_MAPPING_DELIVERY_FIRST_NAME = 'b1_mapping_section/b1_datafields_group/deliveryfirstname';
    const CONFIG_PATH_MAPPING_DELIVERY_LAST_NAME = 'b1_mapping_section/b1_datafields_group/deliverylastname';
    const CONFIG_PATH_MAPPING_DELIVERY_IS_COMPANY = 'b1_mapping_section/b1_datafields_group/deliveryiscompany';
    const CONFIG_PATH_MAPPING_DELIVERY_VAT_CODE = 'b1_mapping_section/b1_datafields_group/deliveryvatcode';
    const CONFIG_PATH_MAPPING_DELIVERY_ADDRESS = 'b1_mapping_section/b1_datafields_group/deliveryaddress';
    const CONFIG_PATH_MAPPING_DELIVERY_CITY = 'b1_mapping_section/b1_datafields_group/deliverycity';
    const CONFIG_PATH_MAPPING_DELIVERY_POST_CODE = 'b1_mapping_section/b1_datafields_group/deliverypostcode';
    const CONFIG_PATH_MAPPING_DELIVERY_COUNTRY = 'b1_mapping_section/b1_datafields_group/deliverycountry';
    const CONFIG_PATH_MAPPING_ITEMS = 'b1_mapping_section/b1_datafields_group/items';
    const CONFIG_PATH_MAPPING_ITEMS_NAME = 'b1_mapping_section/b1_datafields_group/itemsname';
    const CONFIG_PATH_MAPPING_ITEMS_QUANTITY = 'b1_mapping_section/b1_datafields_group/itemsquantity';
    const CONFIG_PATH_MAPPING_ITEMS_PRICE = 'b1_mapping_section/b1_datafields_group/itemsprice';
    const CONFIG_PATH_MAPPING_ITEMS_SUM = 'b1_mapping_section/b1_datafields_group/itemssum';
    const CONFIG_PATH_MAPPING_ITEMS_VAT_RATE = 'b1_mapping_section/b1_datafields_group/itemsvatrate';


    public function _construct()
    {
        $this->_init(ResourceModel\Order::class);
    }

    public function resetOrderReferenceId()
    {
        $this->getResource()->resetOrderReferenceId();
    }

    public function findAllToSync($orderSyncFrom, $statusName,$orderSyncDateTo)
    {
        return $this->getResource()->findAllToSync($orderSyncFrom, $statusName,$orderSyncDateTo);
    }

    public function getValueUsingSql($scopeConfig, $optionStr, $orderId, $itemId = null)
    {
        $sql = $scopeConfig->getValue($optionStr);
        if ($sql == "") {
            return "";
        }
        $query = str_replace("%d0", (string)$orderId, $sql);
        $query = str_replace("%d1", (string)$itemId, $query);
        $query = str_replace("%d", (string)$orderId, $query);

        $result = $this->getResource()->getConnection()->fetchOne($query);
        return $result;
    }

    public function getOrderSyncData($order, $scopeConfig)
    {
        $shopId = $scopeConfig->getValue(static::CONFIG_PATH_SHOP_ID);
        $syncInvoices = $scopeConfig->getValue(static::CONFIG_PATH_ENABLE_SYNC_INVOICES);
        $data = array(
            'shopId' => $shopId,
            'internalId' => $order['entity_id'],
            'date' => date('Y-m-d', strtotime($this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_ORDER_DATE, $order['entity_id']))),
            'number' => strval($this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_ORDER_NO, $order['entity_id'])),
            'currencyCode' => $this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_CURRENCY, $order['entity_id']),
            'shipping' => round((float)$this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_SHIPPING_AMOUNT, $order['entity_id']), 2),
            'gift' => round($this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_GIFT, $order['entity_id']), 2),
            'discount' => round((float)$this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_DISCOUNT, $order['entity_id']), 2),
            'total' => round((float)$this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_TOTAL, $order['entity_id']), 2),
            'email' => $this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_ORDER_EMAIL, $order['entity_id']),
        );

        $data['invoiceSeries'] = null;
        $data['invoiceNumber'] = null;
        if ($syncInvoices > 0) {
            $data['invoiceSeries'] = $this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_INVOICE_SERIES, $order['entity_id']);
            $data['invoiceNumber'] = $this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_INVOICE_NUMBER, $order['entity_id']);
        }

        $billingFirstname = $this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_BILLING_FIRST_NAME, $order['entity_id']);
        $billingLastname = $this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_BILLING_LAST_NAME, $order['entity_id']);
        $data['billing'] = [
            'isJuridical' => empty($this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_BILLING_IS_COMPANY, $order['entity_id'])) ? 0 : 1,
            'name' => $billingFirstname . " " . $billingLastname,
            'vatCode' => $this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_BILLING_VAT_CODE, $order['entity_id']),
            'address' => $this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_BILLING_ADDRESS, $order['entity_id']),
            'cityName' => $this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_BILLING_CITY, $order['entity_id']),
            'postcode' => $this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_BILLING_POST_CODE, $order['entity_id']),
            'countryCode' => $this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_BILLING_COUNTRY, $order['entity_id']),
        ];

        $deliveryFirstname = $this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_DELIVERY_FIRST_NAME, $order['entity_id']);
        $deliveryLastname = $this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_DELIVERY_LAST_NAME, $order['entity_id']);
        $data['delivery'] = [
            'isJuridical' => empty($this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_DELIVERY_IS_COMPANY, $order['entity_id'])) ? 0 : 1,
            'name' => $deliveryFirstname . " " . $deliveryLastname,
            'vatCode' => $this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_DELIVERY_VAT_CODE, $order['entity_id']),
            'address' => $this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_DELIVERY_ADDRESS, $order['entity_id']),
            'cityName' => $this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_DELIVERY_CITY, $order['entity_id']),
            'postcode' => $this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_DELIVERY_POST_CODE, $order['entity_id']),
            'countryCode' => $this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_DELIVERY_COUNTRY, $order['entity_id']),
        ];

        return $data;
    }

    public function getArrayUsingSql($scopeConfig, $optionStr, $orderId)
    {
        $sql = $scopeConfig->getValue($optionStr);
        if ($sql == "") {
            return null;
        }
        $query = str_replace("%d", (string)$orderId, $sql);

        $result = $this->getResource()->getConnection()->fetchAll($query);
        return $result;
    }

    public function getOrderItemSyncData($scopeConfig, $orderId)
    {

        $orderItems = $this->getArrayUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_ITEMS, $orderId);


        $data = [];
        foreach ($orderItems as $i => $item) {
            $data['items'][$i] = array(
                'id' => $item['b1_reference_id'],
                'name' => $this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_ITEMS_NAME, $orderId, $item['item_id']),
                'quantity' => round((float)$this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_ITEMS_QUANTITY, $orderId, $item['item_id']), 3),
                'price' => round((float)$this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_ITEMS_PRICE, $orderId, $item['item_id']), 4),
                'sum' => round((float)$this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_ITEMS_SUM, $orderId, $item['item_id']), 2),
                'vatRate' => (int)$this->getValueUsingSql($scopeConfig, static::CONFIG_PATH_MAPPING_ITEMS_VAT_RATE, $orderId, $item['item_id']),
            );
        }
        return $data['items'];
    }

    public function updateB1ReferenceId($orderId, $referenceId)
    {
        $this->getResource()->updateB1ReferenceId($orderId, $referenceId);
    }

    private function getMappingDefaults()
    {
        return [
            static::CONFIG_PATH_MAPPING_ORDER_DATE => 'SELECT created_at FROM sales_order WHERE entity_id = %d',
            static::CONFIG_PATH_MAPPING_ORDER_NO => 'SELECT entity_id FROM sales_order WHERE entity_id = %d',
            static::CONFIG_PATH_MAPPING_CURRENCY => 'SELECT order_currency_code FROM sales_order WHERE entity_id = %d',
            static::CONFIG_PATH_MAPPING_SHIPPING_AMOUNT => 'SELECT shipping_amount + shipping_tax_amount FROM sales_order WHERE entity_id = %d',
            static::CONFIG_PATH_MAPPING_GIFT => 'SELECT 0',
            static::CONFIG_PATH_MAPPING_DISCOUNT => 'SELECT discount_amount FROM sales_order WHERE entity_id = %d',
            static::CONFIG_PATH_MAPPING_TOTAL => 'SELECT grand_total FROM sales_order WHERE entity_id = %d',
            static::CONFIG_PATH_MAPPING_ORDER_EMAIL => 'SELECT customer_email FROM sales_order WHERE entity_id = %d',
            static::CONFIG_PATH_MAPPING_INVOICE_SERIES => '',
            static::CONFIG_PATH_MAPPING_INVOICE_NUMBER => '',
            static::CONFIG_PATH_MAPPING_BILLING_FIRST_NAME => 'SELECT firstname FROM sales_order p0 LEFT JOIN sales_order_address pm1 ON pm1.entity_id = p0.billing_address_id WHERE p0.entity_id = %d',
            static::CONFIG_PATH_MAPPING_BILLING_LAST_NAME => 'SELECT lastname FROM sales_order p0 LEFT JOIN sales_order_address pm1 ON pm1.entity_id = p0.billing_address_id WHERE p0.entity_id = %d',
            static::CONFIG_PATH_MAPPING_BILLING_IS_COMPANY => 'SELECT company FROM sales_order p0 LEFT JOIN sales_order_address pm1 ON pm1.entity_id = p0.billing_address_id WHERE p0.entity_id = %d',
            static::CONFIG_PATH_MAPPING_BILLING_VAT_CODE => 'SELECT vat_id FROM sales_order p0 LEFT JOIN sales_order_address pm1 ON pm1.entity_id = p0.billing_address_id WHERE p0.entity_id = %d',
            static::CONFIG_PATH_MAPPING_BILLING_ADDRESS => 'SELECT street FROM sales_order p0 LEFT JOIN sales_order_address pm1 ON pm1.entity_id = p0.billing_address_id WHERE p0.entity_id = %d',
            static::CONFIG_PATH_MAPPING_BILLING_CITY => 'SELECT city FROM sales_order p0 LEFT JOIN sales_order_address pm1 ON pm1.entity_id = p0.billing_address_id WHERE p0.entity_id = %d',
            static::CONFIG_PATH_MAPPING_BILLING_POST_CODE => 'SELECT postcode FROM sales_order p0 LEFT JOIN sales_order_address pm1 ON pm1.entity_id = p0.billing_address_id WHERE p0.entity_id = %d',
            static::CONFIG_PATH_MAPPING_BILLING_COUNTRY => 'SELECT country_id FROM sales_order p0 LEFT JOIN sales_order_address pm1 ON pm1.entity_id = p0.billing_address_id WHERE p0.entity_id = %d',
            static::CONFIG_PATH_MAPPING_DELIVERY_FIRST_NAME => 'SELECT firstname FROM sales_order p0 LEFT JOIN sales_order_address pm1 ON pm1.entity_id = p0.shipping_address_id WHERE p0.entity_id = %d',
            static::CONFIG_PATH_MAPPING_DELIVERY_LAST_NAME => 'SELECT lastname FROM sales_order p0 LEFT JOIN sales_order_address pm1 ON pm1.entity_id = p0.shipping_address_id WHERE p0.entity_id = %d',
            static::CONFIG_PATH_MAPPING_DELIVERY_IS_COMPANY => 'SELECT company FROM sales_order p0 LEFT JOIN sales_order_address pm1 ON pm1.entity_id = p0.shipping_address_id WHERE p0.entity_id = %d',
            static::CONFIG_PATH_MAPPING_DELIVERY_VAT_CODE => 'SELECT vat_id FROM sales_order p0 LEFT JOIN sales_order_address pm1 ON pm1.entity_id = p0.shipping_address_id WHERE p0.entity_id = %d',
            static::CONFIG_PATH_MAPPING_DELIVERY_ADDRESS => 'SELECT street FROM sales_order p0 LEFT JOIN sales_order_address pm1 ON pm1.entity_id = p0.shipping_address_id WHERE p0.entity_id = %d',
            static::CONFIG_PATH_MAPPING_DELIVERY_CITY => 'SELECT city FROM sales_order p0 LEFT JOIN sales_order_address pm1 ON pm1.entity_id = p0.shipping_address_id WHERE p0.entity_id = %d',
            static::CONFIG_PATH_MAPPING_DELIVERY_POST_CODE => 'SELECT postcode FROM sales_order p0 LEFT JOIN sales_order_address pm1 ON pm1.entity_id = p0.shipping_address_id WHERE p0.entity_id = %d',
            static::CONFIG_PATH_MAPPING_DELIVERY_COUNTRY => 'SELECT country_id FROM sales_order p0 LEFT JOIN sales_order_address pm1 ON pm1.entity_id = p0.shipping_address_id WHERE p0.entity_id = %d',
            static::CONFIG_PATH_MAPPING_ITEMS => 'SELECT * FROM sales_order_item p0 LEFT JOIN  catalog_product_entity  p1 on p1.sku = p0.sku WHERE p0.order_id  = %d',
            static::CONFIG_PATH_MAPPING_ITEMS_NAME => 'SELECT name FROM sales_order_item p0 WHERE p0.item_id = %d1 AND p0.order_id = %d0',
            static::CONFIG_PATH_MAPPING_ITEMS_QUANTITY => 'SELECT qty_ordered FROM sales_order_item p0 WHERE p0.item_id = %d1 AND p0.order_id = %d0',
            static::CONFIG_PATH_MAPPING_ITEMS_PRICE => 'SELECT price_incl_tax FROM sales_order_item p0 WHERE  p0.item_id = %d1 AND p0.order_id = %d0',
            static::CONFIG_PATH_MAPPING_ITEMS_SUM => 'SELECT row_total_incl_tax FROM sales_order_item p0 WHERE  p0.item_id = %d1 AND p0.order_id = %d0',
            static::CONFIG_PATH_MAPPING_ITEMS_VAT_RATE => 'SELECT tax_percent FROM sales_order_item p0 WHERE  p0.item_id = %d1 AND p0.order_id = %d0',
        ];
    }

    private function getSettingsDefaults()
    {
        return [
            static::CONFIG_PATH_ENABLE_SYNC_INVOICES => 0,
            static::CONFIG_PATH_ENABLE_QUANTITY_SYNC => 1,
            static::CONFIG_PATH_ENABLE_NAME_SYNC => 0,
            static::CONFIG_PATH_ENABLE_PRICE_SYNC => 0,
            static::CONFIG_PATH_ENABLE_WRITEOFF => 0,
            static::CONFIG_PATH_ORDER_SYNC_FROM => date('Y-m-d H:i:s'),
            static::CONFIG_PATH_ORDER_SYNC_TO => null
        ];
    }

    public function resetMappings($configWriter)
    {
        $defaults = $this->getMappingDefaults();
        foreach ($defaults as $k => $value){
            $configWriter->save($k, $value);
        }
        $this->clearCache();
    }

    public function resetSettings($configWriter)
    {
        $defaults = $this->getSettingsDefaults();
        foreach ($defaults as $k => $value){
            $configWriter->save($k, $value);
        }
        $this->clearCache();
    }

    public function clearCache()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cacheTypeList = $objectManager->get(\Magento\Framework\App\Cache\TypeListInterface::class);
        $cacheFrontendPool = $objectManager->get(\Magento\Framework\App\Cache\Frontend\Pool::class);
        $types = array(
            'config',
            'full_page',
        );
        foreach ($types as $type) {
            $cacheTypeList->cleanType($type);
        }
        foreach ($cacheFrontendPool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
    }
}
