<?php

namespace B1\Accounting\Model;

use Magento\Framework\Model\AbstractModel;

class Product extends AbstractModel
{
    protected function _construct()
    {
        $this->_init(ResourceModel\Product::class);
    }

    public function resetAllB1ReferenceId()
    {
        $this->getResource()->resetAllB1ReferenceId();
    }

    public function updateCode($code, $referenceId)
    {
        $this->getResource()->updateCode($code, $referenceId);
    }

    public function updateQuantity($product)
    {
        $this->getResource()->updateQuantity($product);
    }

    public function updateName($product)
    {
        $this->getResource()->updateName($product);
    }

    public function updatePrice($product)
    {
        $this->getResource()->updatePrice($product);
    }

    public function fetchAllItems()
    {
        $entityId = $this->getResource()->fetchAllItems();
        return $entityId;
    }

    public function fetchAllItemCount()
    {
        $count = $this->getResource()->fetchAllItemCount();
        return $count[0]['count'];
    }
}
