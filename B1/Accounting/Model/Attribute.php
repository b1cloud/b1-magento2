<?php

namespace B1\Accounting\Model;

class Attribute implements \Magento\Framework\Data\OptionSourceInterface
{

    public function toOptionArray()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $model = $objectManager->create('\B1\Accounting\Observer\MyObserver');
        $attributes = $model->getImportAttribute();

        $attributeToOption = function ($a) {
            return [
                'value' => $a['id'],
                'label' => __($a['name']),
            ];
        };

        $optionArray = array_map($attributeToOption, $attributes);

        return $optionArray;
    }

}
