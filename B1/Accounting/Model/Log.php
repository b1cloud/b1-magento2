<?php
/**
 * Created by PhpStorm.
 * User: JohnMirro
 * Date: 28.09.2021
 * Time: 14:15
 */

namespace B1\Accounting\Model;

use Magento\Framework\Model\AbstractModel;

class Log extends AbstractModel
{
    const PER_PAGE = 20;

    protected function _construct()
    {
        $this->_init('B1\Accounting\Model\ResourceModel\Log');
    }

    public static function logsTableName()
    {
        return 'b1_logs';
    }

    public function saveLog($is_success, $debug_info)
    {
        $this->getResource()->saveLog($is_success, $debug_info);
    }

    public function clearLogs(){
        $this->getResource()->clearLogs();
    }
}
