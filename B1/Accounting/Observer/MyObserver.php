<?php

namespace B1\Accounting\Observer;

use B1\Accounting\lib\B1\B1;
use B1\Accounting\lib\B1\B1ClientException;
use B1\Accounting\lib\B1\B1DuplicateException;
use B1\Accounting\lib\B1\B1ValidationException;
use Exception;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\ResourceConnection;

ini_set('display_errors', 1);

class MyObserver implements ObserverInterface
{
    const MAX_EXECUTION_TIME = 3600;
    const REQUEST_PATH_ATTRIBUTE = 'reference-book/item-attributes/list';
    const REQUEST_PATH_MEASUREMENT = 'reference-book/measurement-units/list';
    const REQUEST_PATH_IMPORT_ITEMS = 'reference-book/items/create';
    const REQUEST_PATH_CONFIG = 'e-commerce/config/get';
    const REQUEST_PATH_ITEMS = 'e-commerce/items/stock';
    const REQUEST_PATH_CREATE_ORDER = 'e-commerce/orders/create';
    const REQUEST_PATH_DELETE_ORDER = 'e-commerce/orders/delete';
    const REQUEST_PATH_CREATE_ORDER_ITEMS = 'e-commerce/order-items/create';
    const REQUEST_PATH_CREATE_WRITE_OFF = 'e-commerce/orders/create-write-off';
    const REQUEST_PATH_CREATE_SALE = 'e-commerce/orders/create-sale';

    const CONFIG_PATH_API_KEY = 'b1_configuration_section/b1_api_keys_group/api_key';
    const CONFIG_PATH_PRIVATE_KEY = 'b1_configuration_section/b1_api_keys_group/private_key';

    const CONFIG_PATH_MAX_API_REQUEST_PER_SESSION = 'b1_configuration_section/b1_main_config_group/max_requests_to_api_per_session';
    const CONFIG_PATH_ORDER_WRITEOFF = 'b1_configuration_section/b1_main_config_group/enable_write_off';
    const CONFIG_PATH_ENABLE_QUANTITY_SYNC = 'b1_configuration_section/b1_main_config_group/enable_quantity_sync';
    const CONFIG_PATH_ENABLE_NAME_SYNC = 'b1_configuration_section/b1_main_config_group/enable_name_sync';
    const CONFIG_PATH_ENABLE_PRICE_SYNC = 'b1_configuration_section/b1_main_config_group/enable_price_sync';
    const CONFIG_PATH_ORDER_SYNC_FROM = 'b1_configuration_section/b1_main_config_group/order_sync_from';
    const CONFIG_PATH_ORDER_SYNC_TO = 'b1_configuration_section/b1_main_config_group/order_sync_to';
    const CONFIG_PATH_STATUS_NAME = 'b1_configuration_section/b1_main_config_group/order_status_name_check';

    const CONFIG_PATH_ORDERDATE = 'b1_mapping_section/b1_datafields_group/orderdate';

    const ERROR_ACTION_TYPE_NOT_SET = '[error] actionType is not set.';
    const ERROR_ACTION_TYPE_UNKNOWN = '[error] wrong/unknown actionType.';
    const ERROR_FIELD_MAPPING_NOT_SET = '[error] FieldMapping values not set. You need to disable and then enable B1 plugin.';
    const ERROR_VALIDATION = '[error] Validation errors. Please refer to B1 plugin "Data validation" section.';

    const SUCCESS_MSG_CHECK_CONNECTION = '[success] connection works.';
    const SUCCESS_MSG_IMPORT = '[success] imported products to B1.';
    const SUCCESS_MSG_ORDERS_REFERENCE_IDS_RESETED = '[success] orders reference ids reseted.';
    const SUCCESS_MSG_PRODUCTS_SYNCHRONISED = '[success] products synchronisation with B1 success.';
    const SUCCESS_MSG_ORDERS_SYNCHRONISED = '[success] orders synchronisation with B1 success.';
    const SUCCESS_MSG_DEFAULTS_SET = '[success] fields mappings was set to defaults.';

    private $_logger;
    private $_scopeConfig;
    private $_configWriter;
    private $_cacheTypeList;
    private $_request;
    private $_apiKey;
    private $_privateKey;

    private $_orderModel;
    private $_productModel;
    private $_indexerFactory;
    private $_stockRegistryInterface;
    private $_abstractDb;
    private $_b1logger;
    private $_b1_validaton;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \B1\Accounting\Model\Order $orderModel,
        \B1\Accounting\Model\Product $productModel,
        \B1\Accounting\Model\Log $b1Loggger,
        \B1\Accounting\Model\Validation $b1Validaton,
        \Magento\Indexer\Model\IndexerFactory $indexerFactory,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistryInterface,
        ResourceConnection $resourceConnection
    )
    {
        $this->_request = $request;
        $this->_logger = $logger;
        $this->_scopeConfig = $scopeConfig;
        $this->_configWriter = $configWriter;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_orderModel = $orderModel;
        $this->_productModel = $productModel;
        $this->_resourceConnection = $resourceConnection;
        $this->_b1logger = $b1Loggger;
        $this->_b1_validaton = $b1Validaton;

        $this->_apiKey = $this->_scopeConfig->getValue(static::CONFIG_PATH_API_KEY);
        $this->_privateKey = $this->_scopeConfig->getValue(static::CONFIG_PATH_PRIVATE_KEY);
        $this->_indexerFactory = $indexerFactory;
        $this->_stockRegistryInterface = $stockRegistryInterface;
    }

    public function execute(Observer $observer)
    {
        set_time_limit(self::MAX_EXECUTION_TIME);
        ini_set('max_execution_time', self::MAX_EXECUTION_TIME);

        $requestValid = $this->_request->getPost('b1Accounting');
        $actionType = $this->_request->getPost('actionType');

        if (!$requestValid) {
            return;
        }

        $responseMessage = $this->executePostActionType($actionType);

        header('Content-Type: application/json');
        echo json_encode(['result' => $responseMessage]);

        exit;
    }

    public function executePostActionType($actionType)
    {
        if (!isset($actionType)) {
            return static::ERROR_ACTION_TYPE_NOT_SET;
        }

        switch ($actionType) {
            case 'checkConnection':
                return $this->checkConnectionWithB1();
            case 'importItems':
                return $this->importItemsToB1();
            case 'resetOrderReferenceId':
                return $this->resetOrderReferenceId();
            case 'syncProducts':
                return $this->syncB1ProductsWithShop();
            case 'syncOrders':
                return $this->syncShopOrdersWithB1();
            case 'resetMappings':
                return $this->resetMappings();
            case 'resetSettings':
                return $this->resetSettings();
            default:
                return static::ERROR_ACTION_TYPE_UNKNOWN;
        }
    }

    public function checkConnectionWithB1()
    {
        try {
            $b1 = new B1([
                'apiKey' => $this->_apiKey,
                'privateKey' => $this->_privateKey,
            ], $this->_b1logger);

            $b1->request(static::REQUEST_PATH_CONFIG);

        } catch (B1ClientException $e) {
            $errorMessage = static::b1ClientExceptionToErrorMessage($e);
            $this->_logger->error($errorMessage);
            return $errorMessage;

        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            $this->_logger->error($errorMessage);
            return $errorMessage;
        }

        return static::SUCCESS_MSG_CHECK_CONNECTION;
    }

    public function resetOrderReferenceId()
    {
        try {
            $this->_orderModel->resetOrderReferenceId();

        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            $this->_logger->error($errorMessage);
            return $errorMessage;
        }

        return static::SUCCESS_MSG_ORDERS_REFERENCE_IDS_RESETED;
    }

    public function syncB1ProductsWithShop()
    {
        $page = 0;
        $maxRequestsToApi = $this->_scopeConfig->getValue(static::CONFIG_PATH_MAX_API_REQUEST_PER_SESSION);
        $enableQuantitySync = $this->_scopeConfig->getValue(static::CONFIG_PATH_ENABLE_QUANTITY_SYNC);
        $enableNameSync = $this->_scopeConfig->getValue(static::CONFIG_PATH_ENABLE_NAME_SYNC);
        $enablePriceSync = $this->_scopeConfig->getValue(static::CONFIG_PATH_ENABLE_PRICE_SYNC);

        try {
            $b1 = new B1([
                'apiKey' => $this->_apiKey,
                'privateKey' => $this->_privateKey,
            ],$this->_b1logger);

            $this->_productModel->resetAllB1ReferenceId();

            $response = $b1->request(static::REQUEST_PATH_CONFIG);
            $content = $response->getContent();
            $warehouseId = $content['data']['warehouseId'];

            do {
                $page++;
                $response = $b1->request(static::REQUEST_PATH_ITEMS, [
                    'warehouseId' => $warehouseId,
                    'page' => $page,
                    'pageSize' => 500,
                    'filters' => [
                        'groupOp' => 'AND',
                        'rules' => [
                        ],
                    ],
                ]);
                $data = $response->getContent();
                foreach ($data['data'] as $product) {
                    $this->_productModel->updateCode($product['code'], $product['id']);

                    if ($enableQuantitySync) {
                        $this->updateProductQuantity($product);
                    }
                    if ($enableNameSync) {
                        $this->_productModel->updateName($product);
                    }
                    if ($enablePriceSync) {
                        $this->_productModel->updatePrice($product);
                    }
                }
            } while ($page < $data['pages'] && $page < $maxRequestsToApi);

        } catch (B1ClientException $e) {
            $errorMessage = static::b1ClientExceptionToErrorMessage($e);
            $this->_logger->error($errorMessage);
            return $errorMessage;

        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            $this->_logger->error($errorMessage);
            return $errorMessage;
        }

        return static::SUCCESS_MSG_PRODUCTS_SYNCHRONISED;
    }

    public function syncShopOrdersWithB1()
    {

        $testSqlMapping = $this->_scopeConfig->getValue(static::CONFIG_PATH_ORDERDATE);
        if (!$testSqlMapping || $testSqlMapping == "") {
            return static::ERROR_FIELD_MAPPING_NOT_SET;
        }
        $maxRequestsToApi = $this->_scopeConfig->getValue(static::CONFIG_PATH_MAX_API_REQUEST_PER_SESSION);

        $orderSyncDateFrom = $this->_scopeConfig->getValue(static::CONFIG_PATH_ORDER_SYNC_FROM);
        $orderSyncDateTo = $this->_scopeConfig->getValue(static::CONFIG_PATH_ORDER_SYNC_TO);
        $statusName = $this->_scopeConfig->getValue(static::CONFIG_PATH_STATUS_NAME);

        $writeOff = $this->_scopeConfig->getValue(static::CONFIG_PATH_ORDER_WRITEOFF);
        $enableQuantitySync = $this->_scopeConfig->getValue(static::CONFIG_PATH_ENABLE_QUANTITY_SYNC);
        try {
            $b1 = new B1([
                'apiKey' => $this->_apiKey,
                'privateKey' => $this->_privateKey,
            ],$this->_b1logger);

            if ($enableQuantitySync) {
                $response = $b1->request(static::REQUEST_PATH_CONFIG);
                $content = $response->getContent();
                $warehouseId = $content['data']['warehouseId'];
            }

            $orders = $this->_orderModel->findAllToSync($orderSyncDateFrom, $statusName, $orderSyncDateTo);
            if($this->checkOrders($orders)) { // any errors
                return self::ERROR_VALIDATION;
            }

            foreach ($orders as $order) {
                $orderData = $this->_orderModel->getOrderSyncData($order, $this->_scopeConfig);
                try {
                    $result = $b1->request(static::REQUEST_PATH_CREATE_ORDER, $orderData);
                    $data = $result->getContent();
                    $orderId = $data['data']['id'];
                    $this->_orderModel->updateB1ReferenceId($order['entity_id'], $orderId);
                } catch (B1DuplicateException $e) {
                    $content = $e->getResponse()->getContent();
                    $this->_orderModel->updateB1ReferenceId($order['entity_id'], $content['data']['id']);
                    continue;
                } catch (B1ValidationException $e) {
                    $errorMessage = $e->getMessage();
                    $content = $e->getResponse()->getContent();
                    $this->_logger->error($errorMessage);
                    return  "orderId: " . $orderData['internalId'] . " " .  print_r($content,true);
                }


                $itemData = $this->_orderModel->getOrderItemSyncData($this->_scopeConfig, $order['entity_id'],$this->_productModel,$this->_logger);
//                return print_r($itemData, true);
                try {
                    $ids = [];
                    foreach ($itemData as $item) {
                        $item['orderId'] = $orderId;
                        $b1->request(static::REQUEST_PATH_CREATE_ORDER_ITEMS, $item);
                        $ids[] = $item['id'];
                    }
                    if ($writeOff) {
                        $data = [
                            'orderId' => $orderId,
                        ];
                        $b1->request(static::REQUEST_PATH_CREATE_WRITE_OFF, $data);
                    }
                    $data = [
                        'orderId' => $orderId,
                        'series' => $orderData['invoiceSeries'],
                        'number' => $orderData['invoiceNumber'],
                    ];
                    $b1->request(static::REQUEST_PATH_CREATE_SALE, $data);

                    if ($enableQuantitySync) {
                        $page = 0;
                        do {
                            $page++;
                            $data = [
                                'warehouseId' => $warehouseId,
                                'page' => $page,
                                'pageSize' => 500,
                                'filters' => [
                                    'groupOp' => 'AND',
                                    'rules' => [
                                        [
                                            'field' => 'id',
                                            'op' => 'in',
                                            'data' => $ids
                                        ]
                                    ],
                                ],
                            ];
                            $response = $b1->request(static::REQUEST_PATH_ITEMS, $data);
                            $retStocks = $response->getContent();
                            foreach ($retStocks['data'] as $stock) {
                                $this->updateProductQuantity($stock);
                            }
                        } while ($page < $retStocks['pages'] && $page < $maxRequestsToApi);
                    }
                } catch (B1ClientException $e) {
                    $message = $e->getMessage();
                    $content = $e->getResponse()->getContent();

                    $data = [
                        'internalId' => $orderData['internalId'],
                        'shopId' => $orderData['shopId'],
                    ];
                    $b1->request(static::REQUEST_PATH_DELETE_ORDER, $data);
                    $this->_orderModel->updateB1ReferenceId($order['entity_id'], null);

                    return "orderId: " . $orderData['internalId'] . " " . $message . print_r($content, true);
                }
            }

        } catch (B1ClientException $e) {
            $errorMessage = static::b1ClientExceptionToErrorMessage($e);
            $this->_logger->error($errorMessage);
            return $errorMessage;

        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            $this->_logger->error($errorMessage);
            return $errorMessage;
        }

        return static::SUCCESS_MSG_ORDERS_SYNCHRONISED;
    }

    public function getImportAttribute()
    {
        return $this->getImportFields(static::REQUEST_PATH_ATTRIBUTE);
    }

    public function getImportMeasurement()
    {
        return $this->getImportFields(static::REQUEST_PATH_MEASUREMENT);
    }

    private function getImportFields($requestPath)
    {
        try {
            $b1 = new B1([
                'apiKey' => $this->_apiKey,
                'privateKey' => $this->_privateKey,
            ],$this->_b1logger);
            $data = [
                'page' => 1,
                'rows' => 100,
                'filters' => [
                    'groupOp' => 'AND',
                ],
            ];
            $response = $b1->request($requestPath, $data);
            $content = $response->getContent();
            $measurements = $content['data'];
            return $measurements;

        } catch (Exception $e) {
            return [];
        }
    }

    public function importItemsToB1()
    {
        try {
            $maxRequestsToApi = $this->_scopeConfig->getValue(static::CONFIG_PATH_MAX_API_REQUEST_PER_SESSION);
            $attributeId = $_POST['attributeId'];
            $measurementId = $_POST['measurementId'];
            $b1 = new B1([
                'apiKey' => $this->_apiKey,
                'privateKey' => $this->_privateKey,
            ],$this->_b1logger);
            $i = 0;
            $successCount = 0;
            if($this->checkItems($attributeId, $measurementId)) {
                return self::ERROR_VALIDATION;
            }
            do{
                $items = $this->_productModel->fetchAllItems();
                foreach ($items as $item) {
                    $itemData = $this->generateItemData($item, $attributeId, $measurementId);
                    $response = $b1->request(static::REQUEST_PATH_IMPORT_ITEMS, $itemData['data']);
                    $successCount++;
                    $content = $response->getContent();
                    $this->_productModel->updateCode($item['sku'], $content['data']['id']);
                    $i++;
                    if ($i >= $maxRequestsToApi) break;
                }
                if ($i >= $maxRequestsToApi || count($items) == 0) break;
            } while (true);

            return self::SUCCESS_MSG_IMPORT . " Total imports: " . $successCount;

        } catch (B1ClientException $e) {
            $errorMessage = static::b1ClientExceptionToErrorMessage($e);
            $this->_logger->error($errorMessage);
            return $errorMessage;

        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            $this->_logger->error($errorMessage);
            return $errorMessage;
        }
    }

    private function updateProductQuantity($product){
        try{
            $stockItem = $this->_stockRegistryInterface->getStockItemBySku($product['code']);
            $stockItem->setQty($product['quantity']);
            $stockItem->setIsInStock((bool)$product['quantity']);
            $this->_stockRegistryInterface->updateStockItemBySku($product['code'], $stockItem);
        } catch (Exception $e) {}
    }

    private function generateItemData($item, $attributeId, $measurementId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //$product = $objectManager->get('Magento\Catalog\Model\Product')->load($item['entity_id']);
        $product = $objectManager->get('Magento\Catalog\Api\ProductRepositoryInterface')->getById($item['entity_id']);
        $itemData = [];
        $itemData['code'] = $product->getData('sku');
        $itemData['name'] = $product->getData('name');
        $itemData['attributeId'] = $attributeId;
        $itemData['measurementUnitId'] = $measurementId;
        $itemData['priceWithoutVat'] = round($product->getData('price') ?? 0, 2);
        $itemData['description'] = $product->getData('description');

        return [
            'data' => $itemData,
        ];
    }

    public static function b1ClientExceptionToErrorMessage($exception)
    {
        $message = $exception->getMessage();
        $content = $exception->getResponse()->getContent();

        $headersMessages = '';
        if (isset($content['errors']['headers'])) {
            $headersMessages = implode("\n", $content['errors']['headers']);
        }

        $errorMessage =
            $message . "\n" .
            $content['shortMessage'] . ". " .
            $content['message'] .
            print_r($content['errors'],true).
            $headersMessages;

        return $errorMessage;
    }

    public function clearConfigCache()
    {
        $this->_cacheTypeList->cleanType(\Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER);
        $this->_cacheTypeList->cleanType(\Magento\PageCache\Model\Cache\Type::TYPE_IDENTIFIER);
    }

    public function reIndex()
    {
        $connection = $this->_resourceConnection->getConnection();

        $indexers = [
            $connection->getTableName('design_config_grid'),
            $connection->getTableName('customer_grid'),
            $connection->getTableName('catalog_category_product'),
            $connection->getTableName('catalog_product_category'),
            $connection->getTableName('catalogrule_rule'),
            $connection->getTableName('catalog_product_attribute'),
            $connection->getTableName('inventory'),
            $connection->getTableName('catalogrule_product'),
            $connection->getTableName('cataloginventory_stock'),
            $connection->getTableName('catalog_product_price'),
            $connection->getTableName('catalogsearch_fulltext'),
        ];

        foreach ($indexers as $index) {
            $indexer = $this->_indexerFactory->create();
            $indexer->load($index);
            $indexer->reindexAll();
        }
    }

    public function clearOldLogs() {
        $this->_b1logger->clearLogs();
    }

    public function resetMappings()
    {
        try {
            $this->_orderModel->resetMappings($this->_configWriter);
        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            $this->_logger->error($errorMessage);
            return $errorMessage;
        }
        return static::SUCCESS_MSG_DEFAULTS_SET;
    }

    public function resetSettings()
    {
        try {
            $this->_orderModel->resetSettings($this->_configWriter);
        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            $this->_logger->error($errorMessage);
            return $errorMessage;
        }
        return static::SUCCESS_MSG_DEFAULTS_SET;
    }

    /**
     * Check items before sending to B1
     */
    private function checkItems($attributeId, $measurementId)
    {
        $this->_b1_validaton->clearLogs();
        $items = $this->_productModel->fetchAllItems();
        $hasErrors = false;
        $codes = [];
        $requiredItemProperties = ['code','name','attributeId','measurementUnitId'];
        foreach ($items as $item) {
            $itemData = $this->generateItemData($item, $attributeId, $measurementId);
            $errors = [];
            if(isset($itemData['data']['code']) && $itemData['data']['code'] !== '') {
                if (in_array($itemData['data']['code'], $codes)) {
                    $errors[] = "Has duplicate 'code' {$itemData['data']['code']}";
                } else {
                    $codes[] = $itemData['data']['code'];
                }
            }
            foreach ($requiredItemProperties as $property) {
                if(!isset($itemData['data'][$property]) || $itemData['data'][$property] === '') {
                    $errors[] = "Has empty '{$property}' property";
                }
            }

            if(count($errors) > 0) { // save to validation logs
                $hasErrors = true;
                $message = "Item sku '{$item['sku']}' errors: ".implode('. ', $errors);
                $this->_b1_validaton->saveLog($message);
            }
        }
        return $hasErrors;
    }

    /**
     * Check orders before sending to B1
     * @param $orders
     */
    private function checkOrders($orders)
    {
        $this->_b1_validaton->clearLogs();
        $hasErrors = false;
        $requiredOrderProperties = ['shopId','internalId','date','number','currencyCode','discount','total','billing.name','billing.countryCode', 'delivery.name','delivery.countryCode'];
        $requiredItemProperties = ['price','quantity','sum','name',];
        foreach ($orders as $order) {
            $errors = [];
            $orderData = $this->_orderModel->getOrderSyncData($order, $this->_scopeConfig);

            foreach ($requiredOrderProperties as $property) {
                if(strpos($property,'.') !== false) { // nested property
                    $arr = explode('.',$property);
                    $parent = $arr[0];
                    $sub = $arr[1];

                    if (!isset($orderData[$parent][$sub]) || $orderData[$parent][$sub] === '') {
                        $errors[] = "Has empty '{$property}' property";
                    }

                } else {
                    if (!isset($orderData[$property]) || $orderData[$property] === '') {
                        $errors[] = "Has empty '{$property}' property";
                    }
                }
            }

            $itemData = $this->_orderModel->getOrderItemSyncData($this->_scopeConfig, $order['entity_id']);
            foreach ($itemData as $item) {

                foreach ($requiredItemProperties as $property) {
                    if(!isset($item[$property]) || $item[$property] === '') {
                        $errors[] = "Order item has empty '{$property}' property";
                    }
                }
            }

            if(count($errors) > 0) { // save to validation logs
                $hasErrors = true;
                $message = "Order id {$order['entity_id']} errors: ".implode('. ', $errors);
                //$message .= ' '. json_encode($order_data);
                $this->_b1_validaton->saveLog($message);
            }

        }
        return $hasErrors;
    }


}
