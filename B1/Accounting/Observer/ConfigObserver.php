<?php

namespace B1\Accounting\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Psr\Log\LoggerInterface as Logger;

use B1\Accounting\lib\B1\B1;
use B1\Accounting\lib\B1\B1ClientException;
use B1\Accounting\lib\B1\B1DuplicateException;
use B1\Accounting\lib\B1\B1ValidationException;
use Exception;

class ConfigObserver implements ObserverInterface
{
    const CONFIG_PATH_API_KEY = 'b1_configuration_section/b1_api_keys_group/api_key';
    const CONFIG_PATH_PRIVATE_KEY = 'b1_configuration_section/b1_api_keys_group/private_key';


    protected $_b1logger;
    private $_configReader;
    private $_configWriter;

    public function __construct(
        \B1\Accounting\Model\Log $b1Loggger,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
        \Magento\Framework\App\Config\ScopeConfigInterface $configReader
    ) {
        $this->_b1logger = $b1Loggger;
        $this->_configReader = $configReader;
        $this->_configWriter = $configWriter;
    }

    public function execute(Observer $observer)
    {
        $apiKey = $this->_configReader->getValue(static::CONFIG_PATH_API_KEY);
        $privateKey = $this->_configReader->getValue(static::CONFIG_PATH_PRIVATE_KEY);
        if(!$this->checkConnectionWithB1($apiKey,$privateKey))
        {
            // reset bad settings
            $this->_configWriter->save(static::CONFIG_PATH_API_KEY, null);
            $this->_configWriter->save(static::CONFIG_PATH_PRIVATE_KEY, null);

            //clear cache
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $cacheTypeList = $objectManager->get(\Magento\Framework\App\Cache\TypeListInterface::class);
            $cacheFrontendPool = $objectManager->get(\Magento\Framework\App\Cache\Frontend\Pool::class);
            $types = array('config','full_page', );
            foreach ($types as $type) {
                $cacheTypeList->cleanType($type);
            }
            foreach ($cacheFrontendPool as $cacheFrontend) {
                $cacheFrontend->getBackend()->clean();
            }

        }
    }

    public function checkConnectionWithB1($apiKey,$privateKey)
    {
        try {
            $b1 = new B1([
                'apiKey' => $apiKey,
                'privateKey' => $privateKey,
            ], $this->_b1logger);

            $b1->request('e-commerce/config/get');

        } catch (B1ClientException $e) {
            return false;

        } catch (Exception $e) {
            return false;
        }

        return true;
    }

}
