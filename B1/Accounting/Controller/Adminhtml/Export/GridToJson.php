<?php
/**
 * Created by PhpStorm.
 * User: JohnMirro
 * Date: 01.10.2021
 * Time: 10:48
 */

namespace B1\Accounting\Controller\Adminhtml\Export;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use B1\Accounting\Model\Export\ConvertToJSON;
use Magento\Framework\App\Response\Http\FileFactory;

class GridToJson extends Action
{
    /**
     * @var ConvertToJSON
     */
    protected $converter;

    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @param Context $context
     * @param ConvertToJSON $converter
     * @param FileFactory $fileFactory
     */
    public function __construct(
        Context $context,
        ConvertToJSON $converter,
        FileFactory $fileFactory
    ) {
        parent::__construct($context);
        $this->converter = $converter;
        $this->fileFactory = $fileFactory;
    }

    public function execute()
    {
        $filename = 'b1-magento-logs-' . date('Y_m_d_H_i_s').'.json';
        return $this->fileFactory->create($filename, $this->converter->getJSONFile(), 'var');
    }
}
