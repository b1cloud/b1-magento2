<?php
/**
 * Created by PhpStorm.
 * User: JohnMirro
 * Date: 29.09.2021
 * Time: 20:46
 */
namespace B1\Accounting\Controller\Adminhtml\Logs;

class Index extends \Magento\Backend\App\Action
{
    const PAGE_TITLE = 'B1 Accounting Logs';
    const MENU_ID = 'B1_Accounting::logs';

    protected $resultPageFactory = false;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu(static::MENU_ID);
        $resultPage->getConfig()->getTitle()->prepend(__(static::PAGE_TITLE));

        return $resultPage;
    }
}
