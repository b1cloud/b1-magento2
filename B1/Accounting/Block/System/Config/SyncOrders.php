<?php

namespace B1\Accounting\Block\System\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class SyncOrders extends Field
{
    protected $_template = 'B1_Accounting::system/config/syncOrders.phtml';

    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }

    public function getButtonHtml()
    {
        $buttonHTML = $this->getLayout()
            ->createBlock('Magento\Backend\Block\Widget\Button')
            ->setType('button')
            ->setClass('scalable')
            ->setLabel('Sync Orders')
            ->setOnClick('syncOrders()')
            ->toHtml();

        return $buttonHTML;
    }
}
