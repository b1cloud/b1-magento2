<?php

namespace B1\Accounting\Block\System\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Import extends Field
{
    protected $_template = 'B1_Accounting::system/config/import.phtml';

    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }

    public function getButtonHtml()
    {
        $buttonHTML = $this->getLayout()
            ->createBlock('Magento\Backend\Block\Widget\Button')
            ->setType('button')
            ->setClass('scalable')
            ->setLabel('Import')
            ->setOnClick('importItemsToB1()')
            ->toHtml();

        return $buttonHTML;
    }
}
