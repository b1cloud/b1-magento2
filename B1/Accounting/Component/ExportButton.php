<?php
/**
 * Created by PhpStorm.
 * User: JohnMirro
 * Date: 01.10.2021
 * Time: 11:52
 */
namespace B1\Accounting\Component;

class ExportButton extends \Magento\Ui\Component\ExportButton
{
    public function prepare()
    {
        $context = $this->getContext();
        $config = $this->getData('config');
        if (isset($config['options'])) {
            $options = [];
            foreach ($config['options'] as $option) {
                /*Removed xml & csv from here*/
                if($option['value'] != 'xml' && $option['value'] != 'csv'){
                    $additionalParams = $this->getAdditionalParams($config, $context);
                    $option['url'] = $this->urlBuilder->getUrl($option['url'], $additionalParams);
                    $options[] = $option;
                }
            }
            $config['options'] = $options;
            $this->setData('config', $config);
        }
        parent::prepare();
    }
}
